
// [SECTION] MongoDB Aggregation
/*
    - Used to generate, manipulate, and perform operations to create filtered results that can helps us to analyze the data.
*/

// Using the aggregate method:
/*
    - The "$match" is used to pass the document that meet the specified conditions to the next stage/aggregation process.
    - Syntax:
        {$match {field: value}}

    - The "$group" is used to group elements together and field-value pairs of the data from the grouped elements.
    - Syntax:
        {$group: {_id: "value", fieldResult: "valueResult"}}
        
    - Using both $match and $group along with aggregation will find for products are on sale and will group the total amount of stock for all suppliers found.
    - The "$" symbol will refer to a field name that is available in the documents that are being aggregated on.
*/

db.fruits.insertMany([
    {
        name : "Apple",
        color : "Red",
        stock : 20,
        price: 40,
        supplier_id : 1,
        onSale : true,
        origin: [ "Philippines", "US" ]
    },

    {
        name : "Banana",
        color : "Yellow",
        stock : 15,
        price: 20,
        supplier_id : 2,
        onSale : true,
        origin: [ "Philippines", "Ecuador" ]
    },

    {
        name : "Kiwi",
        color : "Green",
        stock : 25,
        price: 50,
        supplier_id : 1,
        onSale : true,
        origin: [ "US", "China" ]
    },

    {
        name : "Mango",
        color : "Yellow",
        stock : 10,
        price: 120,
        supplier_id : 2,
        onSale : false,
        origin: [ "Philippines", "India" ]
    }
]);


db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);

// Field projection with aggregation
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
    {$project: {_id: 0}}
]);

// Sorting aggregated results
/*
    - The "$sort" can be used to change the order of the new aggregated result
    - Syntax:
        {$sort: {field: 1/-1}}
        1 -> lowest to highest
        -1 -> highest to lowest
*/

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
    {$sort: {total: 1}},
]);

// Aggregating results based on array fields
/*
    - The "$unwind" deconstructs an array field from a collection/field with an array value to output a result
    - Syntax:
        {$unwind: field}
*/

db.fruits.aggregate(
    [
        {$unwind: "$origin"},
        {$group: {_id: "$origin", fruits: {$sum: 1}}},
        {$sort: {fruits: 1}}
    ]
);

// [SECTION] Other Aggregate stages
db.fruits.aggregate(
    [
        {$match: {color: "Yellow"}},
        {$count: "Yellow Fruits"}
    ]
);


db.fruits.aggregate(
    [
        {$match: {color: "Yellow"}},
        {$group: {_id: "$color", yellow_fruits_stock: {$avg: "$stock"}}}
    ]
);

/*
let objectName = {
    fieldName1: value,
    fieldName2: function(){
        statement
    },
    fieldName3: value
}
*/

// $min & $max
db.fruits.aggregate(
    [
        {$match: {color: "Yellow"}},
        {$group: {_id: "$color", yellow_fruits_stock: {$min: "$stock"}}},
    ]
);

db.fruits.aggregate(
    [
        {$match: {color: "Yellow"}},
        {$group: {_id: "$color", yellow_fruits_stock: {$max: "$stock"}}},
    ]
);

db.fruits.aggregate(
    [
        {$match: {color: "Yellow"}},
        {$group: {_id: "$color", yellow_fruits_stock: {$max: "$stock"}}},
    ]
);




// sort but no aggregate
// db.fruits.find().sort({name: 1});

